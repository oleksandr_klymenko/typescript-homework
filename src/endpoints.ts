export const ACCOUNT_URL = 'account';
export const FAVORITE_MOVIE_URL = 'account/favorite/movies';
export const SEARCH_MOVIE_URL = 'search/movie';
export const POPULAR_MOVIE_URL = 'movie/popular';
export const TOP_RATED_MOVIE_URL = 'movie/top_rated';
export const UPCOMING_MOVIE_URL = 'movie/upcoming';

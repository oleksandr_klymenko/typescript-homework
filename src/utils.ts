export const makeUrl = (...chunks: string[]): string => chunks.join('/');

export const getRandomNumber = (min: number, max: number): number => Math.floor(Math.random() * (max - min + 1)) + min;

export const getRandomArrayElement = <T>(elements: T[]): T => elements[getRandomNumber(0, elements.length - 1)];

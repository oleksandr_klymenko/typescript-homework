import { env } from '../env';
import { IRequestParams, IApiService } from '../models/api.model';
import { makeUrl } from '../utils';

export class ApiService implements IApiService {
    private get apiUrl(): string {
        return env.baseUrl;
    }

    public get(uri: string, params: IRequestParams = {}): Promise<Response> {
        const options = this.getRequestOptions(params);

        return fetch(makeUrl(this.apiUrl, uri) + options);
    }

    private getRequestOptions(params: IRequestParams): string {
        if (Object.keys(params).length > 0) {
            return '?' + new URLSearchParams(params);
        }

        return '';
    }
}

import { POPULAR_MOVIE_URL, SEARCH_MOVIE_URL, TOP_RATED_MOVIE_URL, UPCOMING_MOVIE_URL } from '../endpoints';
import { env } from '../env';
import { IMovie, IMovieService, IServerMovie } from '../models/movie.model';
import { ApiService } from './api.service';

export class MovieService implements IMovieService {
  private readonly FAVOURITE_MOVIES_KEY = 'FAVOURITE_MOVIES';
  private readonly apiService: ApiService;

  constructor() {
    this.apiService = new ApiService();
  }

  public async search(query: string): Promise<IMovie[]> {
    try {
      const options = { api_key: env.apiKey, query };
      const response = await this.apiService.get(SEARCH_MOVIE_URL, options);
      const res = await response.json();
      console.log(res);
      const hasMovies = res.results && res.results.length;

      if (hasMovies) {
        return this.moviesMapper(res.results);
      }
      throw res;
    } catch (error) {
      console.error(error.status_message);
      return [];
    }
  }

  public async getPopular(): Promise<IMovie[]> {
    try {
      const options = { api_key: env.apiKey };
      const response = await this.apiService.get(POPULAR_MOVIE_URL, options);
      const res = await response.json();
      const hasMovies = res.results && res.results.length;

      if (hasMovies) {
        return this.moviesMapper(res.results);
      }
      throw res;
    } catch (error) {
      console.error(error.status_message);
      return [];
    }
  }

  public async getTopRated(): Promise<IMovie[]> {
    try {
      const options = { api_key: env.apiKey };
      const response = await this.apiService.get(TOP_RATED_MOVIE_URL, options);
      const res = await response.json();
      const hasMovies = res.results && res.results.length;

      if (hasMovies) {
        return this.moviesMapper(res.results);
      }
      throw res;
    } catch (error) {
      console.error(error.status_message);
      return [];
    }
  }

  public async getUpcoming(): Promise<IMovie[]> {
    try {
      const options = { api_key: env.apiKey };
      const response = await this.apiService.get(UPCOMING_MOVIE_URL, options);
      const res = await response.json();
      const hasMovies = res.results && res.results.length;

      if (hasMovies) {
        return this.moviesMapper(res.results);
      }
      throw res;
    } catch (error) {
      console.error(error.status_message);
      return [];
    }
  }

  public getFavourite(): number[] {
    const favMovies: number[] = [];

    if (!localStorage.getItem(this.FAVOURITE_MOVIES_KEY)) {
      return favMovies;
    }

    const temp = JSON.parse(localStorage.getItem(this.FAVOURITE_MOVIES_KEY) as string) as number[];
    favMovies.push(...temp);

    return favMovies;
  }

  public isFavourite(id: number): boolean {
    if (!localStorage.getItem(this.FAVOURITE_MOVIES_KEY)) {
      return false;
    }

    const movies: number[] = JSON.parse(localStorage.getItem(this.FAVOURITE_MOVIES_KEY) as string);

    return movies.some(movieId => movieId === id);
  }

  public addToFavourite(id: number): void {
    const favMovies: number[] = [];

    if (!localStorage.getItem(this.FAVOURITE_MOVIES_KEY)) {
      favMovies.push(id);
    } else {
      const temp = JSON.parse(localStorage.getItem(this.FAVOURITE_MOVIES_KEY) as string) as number[];
      favMovies.push(...temp, id);
    }

    localStorage.setItem(this.FAVOURITE_MOVIES_KEY, JSON.stringify(favMovies))
  }

  public removeFavourite(id: number): void {
    let favMovies: number[] = [];

    if (!localStorage.getItem(this.FAVOURITE_MOVIES_KEY)) {
      localStorage.setItem(this.FAVOURITE_MOVIES_KEY, JSON.stringify(favMovies));
      return;
    }

    const temp = JSON.parse(localStorage.getItem(this.FAVOURITE_MOVIES_KEY) as string) as number[];
    favMovies = temp.filter(movieId => movieId !== id);

    localStorage.setItem(this.FAVOURITE_MOVIES_KEY, JSON.stringify(favMovies));
  }

  private moviesMapper(movies: IServerMovie[]): IMovie[] {
    return movies.map(({ id, overview, title, release_date, poster_path }) => ({
      id,
      overview,
      title,
      release_date,
      poster: `https://image.tmdb.org/t/p/original/${poster_path}`,
    }));
  }
}

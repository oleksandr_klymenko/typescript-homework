export interface IEnv {
  apiKey: string;
  baseUrl: string;
}

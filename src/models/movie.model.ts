export interface IMovieService {
  getPopular(): Promise<IMovie[]>;

  getTopRated(): Promise<IMovie[]>;

  getUpcoming(): Promise<IMovie[]>;

  getFavourite(): number[];

  isFavourite(id: number): boolean;

  addToFavourite(id: number): void;

  removeFavourite(id: number): void;
}

export interface IMovie {
  id: number;
  overview: string;
  title: string;
  release_date: string;
  poster: string;
}

export interface IMoviesResponse {
  page: number;
  results: IServerMovie[];
  total_pages: number;
  total_results: number;
}

export interface IServerMovie {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export enum IMovieType {
  popular = 'popular',
  upcoming = 'upcoming',
  top_rated = 'top_rated',
}

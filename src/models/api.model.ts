export interface IApiService {
	get(uri: string, params: IRequestParams): Promise<Response>;
}

export interface IRequestParams {
  [key: string]: string;
}

export enum IServerStatus {
  OK = 200,
}

export interface IServerError {
  status_code: number;
  status_message: string;
  success: boolean;
}

import { IEnv } from "./models/environment.model";

export const env: IEnv = {
  apiKey: '7b5dbf66ba9ad65bdf7f3abcb0aab9d8',
  baseUrl: 'https://api.themoviedb.org/3'
};

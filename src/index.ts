import { IMovie, IMovieService, IMovieType } from './models/movie.model';
import { MovieService } from './services/movie.service';
import { getRandomArrayElement } from './utils';
import { IFavouriteColor } from './models/color.model';

interface IMovieCardOptions {
  movie: IMovie;
  template: HTMLElement;
  templateClassList: string[];
  service: IMovieService;
}

export async function render(): Promise<void> {
  let selectedMoviesType: IMovieType = IMovieType.popular;
  const movieContainer = document.querySelector('#film-container') as HTMLElement;
  const favouriteContainer = document.querySelector('#favorite-movies') as HTMLElement;
  const navBarToggler = document.querySelector('.navbar-toggler') as HTMLElement;
  const searchBtn = document.querySelector('#submit') as HTMLButtonElement;
  const mainWrapper = document.querySelector('#jsMainWrp') as HTMLElement;
  const buttonWrapper = document.querySelector('#button-wrapper') as HTMLElement;
  const movieTemplate = (document.querySelector('#film-card') as HTMLTemplateElement)
    .content.querySelector('.jsFilmCard') as HTMLElement;
  const randomMovieTemplate = (document.querySelector('#random-film') as HTMLTemplateElement)
    .content.querySelector('#random-movie') as HTMLElement;
  const movieService = new MovieService();
  const movies = await movieService.getPopular();
  const randomMovie = getRandomArrayElement(movies);

  mainWrapper.insertAdjacentElement('afterbegin', showRandomMovie(randomMovie, randomMovieTemplate));
  renderMovies(movies, movieContainer, movieTemplate, ['col-lg-3', 'col-md-4', 'col-12', 'p-2'], movieService);

  buttonWrapper.addEventListener('click', async (event) => {
    const { checked, id } = event.target as HTMLInputElement;

    if (!checked || selectedMoviesType === id) {
      return;
    }

    selectedMoviesType = id as IMovieType;
    let movies: IMovie[];
    switch (selectedMoviesType) {
      case IMovieType.popular:
        movies = await movieService.getPopular();
        break;
      case IMovieType.upcoming:
        movies = await movieService.getUpcoming();
        break;
      case IMovieType.top_rated:
        movies = await movieService.getTopRated();
        break;
    }
    movieContainer.textContent = '';
    renderMovies(movies, movieContainer, movieTemplate, ['col-lg-3', 'col-md-4', 'col-12', 'p-2'], movieService);
  });
  navBarToggler.addEventListener('click', () => {
    const favouriteMovies = movieService.getFavourite()
      .map(movieId => movies.find(movie => movie.id === movieId))
      .filter(movie => !!movie) as IMovie[];
    favouriteContainer.textContent = '';
    renderMovies(favouriteMovies, favouriteContainer, movieTemplate, ['col-12', 'p-2'], movieService);
  });
  searchBtn.addEventListener('click', async () => {
    const searchQuery = (document.querySelector('#search') as HTMLInputElement).value.trim();

    if (searchQuery) {
      const movies = await movieService.search(searchQuery);
      movieContainer.textContent = '';
      renderMovies(movies, movieContainer, movieTemplate, ['col-lg-3', 'col-md-4', 'col-12', 'p-2'], movieService);
    }
  });

  return new Promise(() => {
    //
  });
}

function renderMovies(
  movies: IMovie[],
  container: HTMLElement,
  template: HTMLElement,
  templateClassList: string[],
  movieService: IMovieService): void {
  const fragment = document.createDocumentFragment();

  movies.forEach((movie) => {
    const options: IMovieCardOptions = {
      movie,
      template,
      templateClassList,
      service: movieService,
    };
    const movieElement = generateMovieCard(options);
    fragment.appendChild(movieElement);
  });
  container.appendChild(fragment);
}

function generateMovieCard(options: IMovieCardOptions): HTMLElement {
  const movieElement = options.template.cloneNode(true) as HTMLElement;
  const heartBtn = movieElement.querySelector('.jsHeart') as HTMLElement;

  movieElement.classList.add(...options.templateClassList);
  (movieElement.querySelector('.jsFilmDate') as HTMLElement).textContent = options.movie.release_date;
  (movieElement.querySelector('.jsFilmDesc') as HTMLElement).textContent = options.movie.overview;
  (movieElement.querySelector('.jsFilmPoster') as HTMLImageElement).src = options.movie.poster;
  if (options.service.isFavourite(options.movie.id)) {
    heartBtn.style.fill = IFavouriteColor.favourite;
  } else {
    heartBtn.style.fill = IFavouriteColor.default;
  }

  heartBtn.addEventListener('click', () => {
    if (options.service.isFavourite(options.movie.id)) {
      heartBtn.style.fill = IFavouriteColor.default;
      options.service.removeFavourite(options.movie.id);
      return;
    }
    heartBtn.style.fill = IFavouriteColor.favourite;
    options.service.addToFavourite(options.movie.id);
  });

  return movieElement;
}

function showRandomMovie(movie: IMovie, template: HTMLElement): HTMLElement {
  const movieElement = template.cloneNode(true) as HTMLElement;
  const movieTitle = movieElement.querySelector('#random-movie-name') as HTMLElement;
  const movieDescription = movieElement.querySelector('#random-movie-description') as HTMLElement;

  movieElement.style.background = `url('${movie.poster}') no-repeat center / cover`;
  movieTitle.textContent = movie.title;
  movieDescription.textContent = movie.overview;

  return movieElement;
}
